var fs = require('fs');
var res = {};

fs.readFile("all.txt", function(err, data){
	if (err){
		throw err;
	}
	parse(data.toString())
});

function parse(data){
	var rows = data.split("\n").filter(function(value){		
		return (value.substr(1,7)=="setting");	
	});
	
	rows.forEach(function(row, index){
		
		var key = row.slice(row.indexOf('[') + 1, row.indexOf(']'));
		var value = row.slice(row.indexOf('=') + 2);
		
		if (key == '' || value == '') return;

		if (!res[key]) {
			res[key] = [];
		}
		if (!res[key].some(function(v){ return v == value})) res[key].push(value);
	});
	
	var settingsMd = "# Settings #\n\n";
	var arr = []
	for (var key in res){
		arr.push(key);	 
	}
	settingsMd += arr.sort()
		.map(function(k){ return "* [" + k + "](" + k + ")";})
		.join('\n\n');
	settingsMd += "\n\n[Home](Home)";
	fs.writeFile("..\\V6_JSP_APY_wiki\\Settings.md", settingsMd);
		
	for (var key in res){
		var md = "# " + key + " #\n\n";
		md += res[key].sort().map(function(v){return "* " + v;}).join('\n\n');
		md += "\n\n[Settings](Settings)\n\n[Home](Home)\n\n"
		fs.writeFileSync("..\\V6_JSP_APY_wiki\\" + key + ".md", md);
	}
}