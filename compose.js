var fs = require('fs');

fs.readdir("./", function (err, files) {
	if (err) throw err;
	files.forEach(function (file) {
		if (file.substr(-4) != ".jsp") return;
		fs.readFile(file, function (err, data) {
			if (err) throw err;
			compose(file, data.toString());
		});
	});
});

fs.readdir("./", function (err, files) {
	if (err) throw err;
	files = files.filter(function(file){return file.substr(-4) == ".jsp";});
	files = files.map(function(file){return "* [" + file + "]" + "(" + file + ")"});
	fs.writeFile("..\\V6_JSP_APY_wiki\\JSPs.md", files.join('\n'));
});

function compose(file, data) {
	var rows = data.split('\n');
	rows = rows.map(function (row) {
		row = row.slice(row.indexOf('?') + 1);
		var slashIndex = row.indexOf('\\');
		if (slashIndex > 0) {
			return row.slice(0, slashIndex);
		}
		return row;
	});

	var table = {};
	rows.forEach(function (row) {
		var keyValues = row.split('&').sort();
		keyValues.forEach(function (keyValue) {
			var kv = keyValue.split('=');
			if (kv.length != 2) return;
			var key = kv[0];
			var value = kv[1];
			if (value.trim() == "") return;
			if (!table[key]) table[key] = [];
			if (!table[key].some(function (v) { return v == value; })) table[key].push(value);
		});
	});

	
	var keys = [];
	for (var key in table) {
		keys.push(key);
	}
	keys = keys.sort();
	var dic = keys.map(function(key){return "* *" + key + "*"});

	var body = [];	
	for (var key of keys) {
		var values = table[key];
		values = values.sort().map(function (value) { return '* ' + value; })
		body.push("### " + key + " ###" + '\n' + values.join('\n\n'));
	}
	var md = "# " + file + " #\n\n" + dic.join('\n') + "\n\n" + body.join('\n\n') + "\n\n[JSPs](JSPs)\n\n[Home](Home)";
	fs.writeFile("..\\V6_JSP_APY_wiki\\" + file + ".md", md);
}