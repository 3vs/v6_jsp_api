var fs = require('fs');
var res = {};

fs.readFile("all.txt", function(err, data){
	if (err){
		throw err;
	}
	parse(data.toString())
});

function parse(data){
	var rows = data.split("\n").filter(function(value){		
		return (value.substr(1,4)=="href");	
	});
	
	rows.forEach(function(row, index){
		var emxIndex = row.indexOf("emx");
		var qIndex = row.indexOf("?");
		if (emxIndex < 0 || qIndex < 0) {
			return;
		}
		var pageName = row.substr(emxIndex, (qIndex - emxIndex));
		if (pageName.length == 0){
			return;
		}
		if (!res[pageName]) {
			res[pageName] = [];
		}
		res[pageName].push(row);
	});
		
	for (var page in res){
		var txt = res[page].map(function(value){
			return value.slice(1);
		}).join("\n");
		fs.writeFile(page, txt);
	}
}